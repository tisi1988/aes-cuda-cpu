/*
	Copyright 2012 Ruben Sanchez Castellano

	This file is part of the final degree project called "ESTUDI DE VIABILITAT 
	DE LA TECNOLOGIA CUDA PER L’ACCELERACIo DE PROCESSOS" ("FEASIBILITY STUDY 
	OF CUDA TECHNOLOGY FOR PROCESSES ACCELERATION") authored by 
	Ruben Sanchez Castellano. This project is also entirely	available at:
	<http://upcommons.upc.edu/pfc/handle/2099.1/15165>.

	AES Cuda-CPU is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AES Cuda-CPU is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

// Flag needed to support files grater than 2GB
#define _FILE_OFFSET_BITS 64

// FIX: Solves Windows warning when opening files with fopen()
#ifdef _WIN32
#define _CRT_SECURE_NO_DEPRECATE
#define Pause() system("PAUSE");
#else
#define fopen fopen64
#define Pause() {}
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <cuda.h>

// Amount of CUDA blocks to launch
#define CUDA_BLOCKS 65535
// Amount of CUDA threads
#define CUDA_THREADS_PER_BLOCK 300
// Amount of data loaded from disk at once.
// NOTE: This depends on the amount of free RAM on the device at the moment of running
// this application. To be safe, load only almost 300MB per loop.
// TODO Compute this dynamically on each loop.
#define NUM_STATE_BUFFER CUDA_BLOCKS * CUDA_THREADS_PER_BLOCK

#include "../data_types.h"
#include "../aes.h"

// So define the data buffer length as the max number of matrix times its size
#define MAX_BUFFER_LENGTH STATE_MATRIX_SIZE * NUM_STATE_BUFFER

#include "../io.h"

/**
 * Prints an error and exits.
 * @param inFile The file to process.
 * @param outFile The output file
 * @param oFilename The output filename
 */
void EndWithError(FILE* inFile, FILE* outFile, char* oFilename) {
    printf("\nError raised, the program finishes now\n");
    // Close the files, remove the output file and exit
    fclose(inFile);
    fclose(outFile);
    remove(oFilename);
    Pause();
    exit(EXIT_SUCCESS);
}

/**
 * AES initialization function.
 * @param op The operation character the user introduced on the program execution command. 'e' for encrypt and 'd' for decrypt.
 * @param KeyLong The key length. 1 for 128 bits, 2 for 192 bits and 3 for 256 bits
 * @param inputFile The file to get the data from.
 * @param outputFile The file to put the processed data on.
 * @param keyFile The file with the key value.
 * @param inputFilename The filename of the input file.
 * @param outputFilename The filename of the output file.
 * @param keyFilename The filename of the key file.
 */
void initAES(char* op, char* KeyLong, FILE** inputFile,
        FILE** outputFile, FILE* keyFile, char* inputFilename,
        char* outputFilename, char* keyFilename) {

    int expkey_it, klong = 0;

    /* Check the operation value */
    if (*op != 'd' && *op != 'e') {
        printf("Unknown operation\n");
        Pause();
        exit(EXIT_SUCCESS);
    } else {
        /* If the value is correct, set the operation */
        CYPHER_OP = (*op == 'd');
    }

    /* From the key length value (128, 192 or 256 bits) set the values for
     * the expanded key length and the necessary AES g_cypher_rounds. */
    if (*KeyLong == '1') {
        g_aes_key_size_bits = 128;
        g_cypher_rounds = 10;
        g_key_word_amount = 4;
    } else if (*KeyLong == '2') {
        g_aes_key_size_bits = 192;
        g_cypher_rounds = 12;
        g_key_word_amount = 6;
    } else if (*KeyLong == '3') {
        g_aes_key_size_bits = 256;
        g_cypher_rounds = 14;
        g_key_word_amount = 8;
    } else {
        printf("Key length unknown \n");
        Pause();
        exit(EXIT_SUCCESS);
    }

    /* Files initialization*/
    printf("\nOpening files...\n");
    keyFile = fopen(keyFilename, "rb");
    *inputFile = fopen(inputFilename, "rb");
    if (keyFile == NULL) {
        printf("Error opening file \"%s\" for key reading \n", keyFilename);
        Pause();
        exit(EXIT_SUCCESS);
    } else {
        printf("File \"%s\" opened successfully for key reading\n", keyFilename);
    }
    if (*inputFile == NULL) {
        printf("Error opening the input file \"%s\"\n", inputFilename);
        Pause();
        exit(EXIT_SUCCESS);
    } else {
        printf("Input file \"%s\" opened successfully\n", inputFilename);
    }

    /*Create the output file. If the name given is the same as the input, add a suffix.*/
    if (strcmp(inputFilename, outputFilename) == 0) {
        printf("Output file name must be different, adding suffix\n: \"%s.out\"\n", outputFilename);
        *outputFile = fopen(strcat(outputFilename, ".out"), "wb");
    } else {
        *outputFile = fopen(outputFilename, "wb");
    }
    if (outputFile == NULL) {
        printf("Error creating the output file\n");
        fclose(*inputFile);
        fclose(keyFile);
        exit(EXIT_SUCCESS);
    } else {
        printf("Output file \"%s\" created and opened successfully\n", outputFilename);
    }
    /*Key reading*/
    printf("\nReading key...\n");
    klong = ReadKey(g_original_key, keyFile);
    if (klong != g_aes_key_size_bits >> 3) {
        printf("Key length expected %d bytes, key length read %d bytes\n", g_aes_key_size_bits >> 3, klong);
        EndWithError(*inputFile, *outputFile, outputFilename);
    }
    printf("Key read form the file: \n");
    for (expkey_it = 0; expkey_it < g_aes_key_size_bits >> 3; expkey_it++) {
        printf("%02x ", g_original_key[expkey_it]);
        if (expkey_it == (g_aes_key_size_bits >> 3) - 1) printf("\n");
    }

    /*Key expansion*/
    printf("\nExpanding key...\n");
    KeyExpansion(g_original_key);

    printf("AES initialized successfully\n\n");
}

void closeAES(FILE* in, FILE * out) {
    fclose(in);
    fclose(out);
}


/**
 * Kernel that executes the AES algorithm on a state matrix.
 * 
 * @param State The state matrix to process.
 * @param operation The operation to process (ENCRYPT or DECRYPT).
 */
__global__ void ExecuteAES(byte operation, byte* gpuBuffer, byte* expanded_key, int nStates, int g_cypher_rounds) {
    byte state[4][4];

    int dump_it;
    int thread_idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (thread_idx < nStates) {
        // In the CUDA version, the State matrix is kernel local, so copy the data form the device shared memory into the local matrix
        for (dump_it = 0; dump_it < 16; ++dump_it) {
            state[dump_it >> 2][dump_it % 4] = gpuBuffer[thread_idx << 4 + dump_it];
        }

		AES_ROUND(CUDA_SUFFIX)

        // Copy back the processed data to the device's shared memory
        for (dump_it = 0; dump_it < 16; ++dump_it) {
            gpuBuffer[thread_idx >> 4 + dump_it] = state[dump_it >> 2][dump_it % 4];
        }

    }
    // Wait till all threads are done
    __syncthreads();

}

// Print device properties
void printDevProp(cudaDeviceProp prop)
{
    printf("  Device name: %s\n", prop.name);
    printf("  Compute capability: %d.%d\n\n", prop.major, prop.minor);
    printf("  Clock Rate: %d kHz\n", prop.clockRate);
    printf("  Total SMs: %d \n", prop.multiProcessorCount);
    printf("  Shared Memory Per SM: %lu bytes\n", prop.sharedMemPerMultiprocessor);
    printf("  Registers Per SM: %d 32-bit\n", prop.regsPerMultiprocessor);
    printf("  Max threads per SM: %d\n", prop.maxThreadsPerMultiProcessor);
    printf("  L2 Cache Size: %d bytes\n", prop.l2CacheSize);
    printf("  Total Global Memory: %lu bytes\n", prop.totalGlobalMem);
    printf("  Memory Clock Rate: %d kHz\n\n", prop.memoryClockRate);
    printf("  Max threads per block: %d\n", prop.maxThreadsPerBlock);
    printf("  Max threads in X-dimension of block: %d\n", prop.maxThreadsDim[0]);
    printf("  Max threads in Y-dimension of block: %d\n", prop.maxThreadsDim[1]);
    printf("  Max threads in Z-dimension of block: %d\n\n", prop.maxThreadsDim[2]);
    printf("  Max blocks in X-dimension of grid: %d\n", prop.maxGridSize[0]);
    printf("  Max blocks in Y-dimension of grid: %d\n", prop.maxGridSize[1]);
    printf("  Max blocks in Z-dimension of grid: %d\n\n", prop.maxGridSize[2]);
    printf("  Shared Memory Per Block: %lu bytes\n", prop.sharedMemPerBlock);
    printf("  Registers Per Block: %d 32-bit\n", prop.regsPerBlock);
    printf("  Warp size: %d\n\n", prop.warpSize);
}

int main(int argc, char** argv) {

    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, 0);
    printDevProp(prop);

    size_t free,total;
    cudaMemGetInfo(&free, &total);
    printf("Free: %ld Bytes / %ld Bytes\n", free, total);

    /* Iterators */
    int bytesRead, bytesWritten;

    /* Number of state matrix stored on the buffer*/
    unsigned long nStatesInBuffer = 0L;

    /* Counters for metrics */
    int hdd_cont = 0;
    unsigned long processedBytes=  0L;

    /* File pointers */
    FILE *inFile, *outFile, *keyFile = NULL;

    /* Pointers to device data buffer and key buffer */
    byte *gpuDataBuffer, *gpuExpKeyBuffer;

    /* Clock variables to show the time elapsed */
    clock_t clockCounter;
    unsigned long totalTime = 0L;

    /* CUDA error variable */
    cudaError_t cuda_error;

    if (argc == 6) {
        initAES(argv[1], argv[2], &inFile, &outFile, keyFile, argv[3], argv[4], argv[5]);

        /* Memory allocation on the CUDA device to store the data and expanded key buffers*/
        cuda_error = cudaMalloc((void**) &gpuDataBuffer, sizeof (byte) * MAX_BUFFER_LENGTH);
        cuda_error = cudaMalloc((void**) &gpuExpKeyBuffer, sizeof (byte) * 60 * 4);
        if (cuda_error != cudaSuccess) {
            printf("Error %d allocating memory on the CUDA device for data and expanded key buffers\n", cuda_error);
            EndWithError(inFile, outFile, argv[4]);
        } else {
            printf("Memory successfully allocated on the CUDA device for the data and expanded key buffers\n");
        }

        /* Copy the expanded key to the CUDA device memory */
        cuda_error = cudaMemcpy(gpuExpKeyBuffer, g_expanded_key, sizeof (byte) * 60 * 4, cudaMemcpyHostToDevice);
        if (cuda_error != cudaSuccess) {
            printf("Error copying the expanded key to the CUDA device\n");
            EndWithError(inFile, outFile, argv[4]);
        } else {
            printf("Expanded key successfully copied to the GPU memory\n\n");
        }

        /* Load from the file and process it*/
        bytesRead = LoadDataBuffer(inFile);
        while (bytesRead > 0) {
            printf("Processing data from buffer                                   \r");
            fflush(stdout);

            hdd_cont++;

            /* Update the number of state matrix on the buffer*/
            nStatesInBuffer = bytesRead / 16;
            if (bytesRead % 16 != 0) {
                /* If the number of bytes read is not divisible by 16, insert
                 the rest of the bytes and add padding bytes.*/
                nStatesInBuffer++;
                printf("Padding needed!! Setting %ld bytes to %0x02x\n", ((nStatesInBuffer * 16) - bytesRead), (byte)((nStatesInBuffer * 16) - bytesRead));
                memset(io_buffer + bytesRead, ((nStatesInBuffer * 16) - bytesRead), ((nStatesInBuffer * 16) - bytesRead));
            }

            printBuffer(io_buffer, 16);

            /* Start timing */
            clockCounter = clock();

            /* Copy data from RAM to CUDA device's memory */
            cudaMemcpy(gpuDataBuffer, io_buffer, sizeof (byte) * MAX_BUFFER_LENGTH, cudaMemcpyHostToDevice);
            if (cuda_error != cudaSuccess) {
                printf("Error copying data to CUDA device\n");
                EndWithError(inFile, outFile, argv[4]);
            }

            /* Launch the kernel run on the CUDA device */
            ExecuteAES << <CUDA_BLOCKS, CUDA_THREADS_PER_BLOCK >> >(CYPHER_OP, gpuDataBuffer, gpuExpKeyBuffer, nStatesInBuffer, g_cypher_rounds);

            /* Copy the processed data back from the CUDA device's memory to RAM */
            cudaMemcpy(io_buffer, gpuDataBuffer, sizeof (byte) * MAX_BUFFER_LENGTH, cudaMemcpyDeviceToHost);
            if (cuda_error != cudaSuccess) {
                printf("Error retrieving processed data from the CUDA device\n");
                EndWithError(inFile, outFile, argv[4]);
            }
            printf("Data buffer processed in %f seconds\n", ((double) clock() - clockCounter) / CLOCKS_PER_SEC);

            printBuffer(io_buffer, 16);

            // Write the buffer to the output file
            bytesWritten = WriteBuffer(io_buffer, nStatesInBuffer, outFile, (*argv[1] == 'd') ? DECRYPT_OPERATION_CODE : ENCRYPT_OPERATION_CODE);
            if (bytesWritten < nStatesInBuffer) {
                printf("Error writing the buffer on the output file!!\n");
                EndWithError(inFile, outFile, argv[4]);
            }

            hdd_cont++;

            // Update the number of bytes processed
            processedBytes += bytesRead;

            // Read the next bunch of data
            bytesRead = LoadDataBuffer(inFile);
        }

        /* Free CUDA device's memory */
        cuda_error = cudaFree(gpuDataBuffer);
        if (cuda_error != cudaSuccess) {
			printf("Error %d freeing the CUDA device's memory for data buffer\n", cuda_error);
		}
        cuda_error = cudaFree(gpuExpKeyBuffer);
        if (cuda_error != cudaSuccess) {
            printf("Error %d freeing the CUDA device's memory for key\n", cuda_error);
        }

        closeAES(inFile, outFile);
        printf("\n\nPROCESS FINISHED!!\n");
        printf("Processed: %lu bytes \nHDD I/O operations: %d I/Os\n", processedBytes, hdd_cont);
        printf("Time elapsed: %lu seconds (aprox).\n", totalTime);
        Pause();
        return (EXIT_SUCCESS);
    } else {
        printf("Execution command: '$>./AES <operation> <key_length> <input_file> <output_file> <key_file>'\n");
    }
}
