/*
	Copyright 2012 Ruben Sanchez Castellano

	This file is part of the final degree project called "ESTUDI DE VIABILITAT 
	DE LA TECNOLOGIA CUDA PER L’ACCELERACIo DE PROCESSOS" ("FEASIBILITY STUDY 
	OF CUDA TECHNOLOGY FOR PROCESSES ACCELERATION") authored by 
	Ruben Sanchez Castellano. This project is also entirely	available at:
	<http://upcommons.upc.edu/pfc/handle/2099.1/15165>.

	AES Cuda-CPU is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AES Cuda-CPU is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

typedef unsigned char byte;

#define DWORD_SIZE sizeof(unsigned short)
