/*
	Copyright 2012 Ruben Sanchez Castellano

	This file is part of the final degree project called "ESTUDI DE VIABILITAT 
	DE LA TECNOLOGIA CUDA PER L’ACCELERACIo DE PROCESSOS" ("FEASIBILITY STUDY 
	OF CUDA TECHNOLOGY FOR PROCESSES ACCELERATION") authored by 
	Ruben Sanchez Castellano. This project is also entirely	available at:
	<http://upcommons.upc.edu/pfc/handle/2099.1/15165>.

	AES Cuda-CPU is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AES Cuda-CPU is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

// The data buffer which stores the data to process
byte io_buffer[MAX_BUFFER_LENGTH];

/**
 * Read a file and stores the data into the data buffer.
 * @param inFile The file to read (already opened).
 * @return  The number of bytes read
 */
int LoadDataBuffer(FILE* inputFile) {
    int bytesRead = 0;
    // Check if the entire file has been consumed
    if(feof(inputFile)) {
	return bytesRead;
    }
    // Try to read the buffer size
    bytesRead = fread(io_buffer, 1, MAX_BUFFER_LENGTH, inputFile);

    printf("Loading the input file...\r");
    fflush(stdout);

    if (bytesRead > 1024 ) {
         printf("Buffer loaded (%d KB read)                                 \n", bytesRead / 1024);
    } else if (bytesRead > 0) {
        printf("Buffer loaded (%d B read)                                  \n", bytesRead);
    } else {
        printf("Buffer not loaded, empty file?                                      \n");
    }

    return bytesRead;
}

/**
 * Read a file and stores the data into the data buffer.
 * @param outBuffer The buffer to write
 * @param nStatesInBuffer The amount of state matrix inside the buffer
 * @param outFile File pointer to the output file
 * @param inv Operation code
 * @return  The number of bytes written
 */
int WriteBuffer(byte outBuffer[], int nStatesInBuffer, FILE * outFile, byte inv) {
    int nPaddingBytes = 0, lastByte = (nStatesInBuffer - 1)*16 + 15, bytesWritten;
    // Get the last byte from the data
    byte byte_padding = outBuffer[lastByte];

    /* Check the last bytes to detect the number of padding bytes on the buffer.
     * This operation its only done while DECRYPT, and repeated value means its
     * a padding byte.*/
    for (; inv == DECRYPT_OPERATION_CODE && lastByte > 0 && byte_padding == outBuffer[lastByte]; lastByte--) {
        nPaddingBytes++;
    }

    if (nPaddingBytes > 1 || (nPaddingBytes == 1 && byte_padding == 0x01))
    	printf("Detected %d padding bytes\n", nPaddingBytes);
    else
    	nPaddingBytes = 0;

    printf("Writing data from buffer into the output file...\n");
    fflush(stdout);

    int bytesToWrite = nPaddingBytes > 0 ? nStatesInBuffer * 16 - nPaddingBytes : nStatesInBuffer * 16;
    bytesWritten = fwrite(outBuffer, 1, bytesToWrite, outFile);
    if (bytesWritten > 1024) {
        printf("Bytes written (%d KB)                           \n\n", bytesWritten / 1024);
    } else if (bytesWritten > 0) {
        printf("Bytes written (%d B)                           \n\n", bytesWritten);
    } else {
        printf("Nothing has been written on the output file  \n\n");
    }
    return bytesWritten;
}

/**
 * Read the key from the file.
 * @param Key Array to store the key.
 * @param KeyFile The file where the key is stored.
 * @return The key length
 */
int ReadKey(byte Key[], FILE * KeyFile) {
    int key_it = fread(Key, 1, 32, KeyFile);
    fclose(KeyFile);
    return key_it;
}
